import React from 'react'
import { NavLink , Link , withRouter} from 'react-router-dom'
 let Header = ({history}) => {
  let onClickLogOut = () => {
    localStorage.removeItem('token')
    history.push('/login')
  }  
  return (
    <div className="header-app">
        <NavLink exact className="nav-link" activeClassName="nav-link-active" to='/'>Dashboard</NavLink>
        <NavLink className="nav-link" activeClassName="nav-link-active" to='/Members'>Members</NavLink>
        <button className="btn-logout" onClick={onClickLogOut}>Logout</button>
    </div>
  )
}

export default withRouter(Header)