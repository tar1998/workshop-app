import Header from "./Header";
import React, { Component } from 'react'

export default (Component) => {
  return class Layout extends Component {
    render() {
      return (
        <div>
          <Header/>
          <Component/>
        </div>
      )
    }
  }

}


